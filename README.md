# chord.lol

Webassembly component for calculating chords in a given scale. 

Made with [yew framework](https://yew.rs)

[Live page](https://alamminsalo.gitlab.io/chordlol/)