import loader from './chordlol.es6.js'

// mod loading fn
const load_mod = async (mod_loader, mod_uri) => {
  const mod = mod_loader()
  const { instance } = await WebAssembly.instantiateStreaming(fetch(mod_uri), mod.imports)
  return mod.initialize(instance)
}

// init fn
(async () => {
  const chordlol = await load_mod(loader, './chordlol.wasm')
  chordlol.mount_to(document.getElementById('root'))
})()
