use failure::Error;
use serde_derive::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, PartialEq, Clone)]
pub struct State {
    pub root: String,
    pub scale: String,
    pub oscillator: String,
}

impl Default for State {
    fn default() -> Self {
        State {
            root: "C".into(),
            scale: "major".into(),
            oscillator: "sine".into(),
        }
    }
}

impl Into<Result<String, Error>> for State {
    fn into(self) -> Result<String, Error> {
        serde_json::to_string(&self).map_err(|e| e.into())
    }
}

impl From<Result<String, Error>> for State {
    fn from(v: Result<String, Error>) -> Self {
        serde_json::from_str(&v.unwrap_or("".into())).unwrap_or(State::default())
    }
}
