use chordlol;

#[macro_use]
extern crate stdweb;

use chordlol::component::Root;
use stdweb::web::Element;
use yew::App;

#[js_export]
fn mount_to(el: Element) {
    let app = App::<Root>::new();
    app.mount(el);
}

// nothing on main
fn main() {}
