use super::tile::Tile;
use crate::model::State;
use chords::chord::Chord;
use yew::{html, Callback, Component, ComponentLink, Html, Renderable, ShouldRender};

pub struct Grid {
    state: State,
    notes: Vec<String>,
    chords: Vec<Chord>,
    on_tile_clicked: Option<Callback<()>>,
    on_state_change: Option<Callback<State>>,
}

pub enum Msg {
    TileClicked(Chord),
}

#[derive(PartialEq, Clone)]
pub struct Prop {
    pub on_tile_clicked: Option<Callback<()>>,
    pub on_state_change: Option<Callback<State>>,
    pub state: State,
}

impl Default for Prop {
    fn default() -> Self {
        Self {
            state: State::default(),
            on_tile_clicked: None,
            on_state_change: None,
        }
    }
}

// Shorthand to get chord list and notes
fn get_chords(root: &str, scale: &str) -> (Vec<String>, Vec<Chord>) {
    chords::analyze(root, scale, true)
}

impl Component for Grid {
    type Message = Msg;
    type Properties = Prop;

    fn create(prop: Self::Properties, _: ComponentLink<Self>) -> Self {
        let (notes, chords) = get_chords(&prop.state.root, &prop.state.scale);
        Grid {
            state: prop.state,
            on_tile_clicked: prop.on_tile_clicked,
            on_state_change: prop.on_state_change,
            chords,
            notes,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            _ => false,
        }
    }

    fn change(&mut self, prop: Self::Properties) -> ShouldRender {
        let changed = self.state != prop.state;
        self.state = prop.state;
        let (notes, chords) = get_chords(&self.state.root, &self.state.scale);
        self.chords = chords;
        self.notes = notes;
        self.on_tile_clicked = prop.on_tile_clicked;
        self.on_state_change = prop.on_state_change;

        changed
    }
}

impl Renderable<Grid> for Grid {
    fn view(&self) -> Html<Self> {
        // get first of each group of chords
        let chords = self.notes.iter().map(|note| {
            self.chords
                .iter()
                .filter(|chord| chord.notes[0] == note.to_lowercase())
                .map(|c| c.clone())
                .collect()
        });

        html! {
            <div uk-grid="",
                class="uk-padding-small uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-5@m uk-child-width-1-6@l",>
                { for chords.enumerate().map(|(index, chords)| view_tile(chords, index)) }
            </div>
        }
    }
}

fn view_tile(chords: Vec<Chord>, index: usize) -> Html<Grid> {
    html! {
        <Tile: chords=chords, index=index, on_clicked=|c| Msg::TileClicked(c), />
    }
}
