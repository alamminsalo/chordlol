/**
 * Modal dialog for information of symbol
 */
use crate::model::{History, Interval, Point, State, Ticker};
use failure::Error;
use math::round;
use serde_derive::{Deserialize, Serialize};
use stdweb::unstable::TryInto;
use yew::{
    format::json::Json,
    format::nothing::Nothing,
    html,
    services::console::ConsoleService,
    services::fetch::{FetchService, FetchTask, Request, Response},
    Callback, Component, ComponentLink, Html, Renderable, ShouldRender,
};

#[derive(Serialize, Deserialize)]
struct HistoryData {
    data: Vec<History>,
}

pub struct Modal {
    state: State,
    ticker: Ticker,
    fetch: FetchService,
    change_pct: f32,
    link: ComponentLink<Self>,
    console: ConsoleService,
    task: Option<FetchTask>,
    on_close: Option<Callback<()>>,
    on_state_update: Option<Callback<State>>,
}

pub enum Msg {
    Load,
    Loaded(Vec<History>),
    Error(String),
    Close,
    Pin,
    IntervalSelect(Interval),
}

#[derive(PartialEq, Clone)]
pub struct Prop {
    pub state: State,
    pub data: Ticker,
    pub on_close: Option<Callback<()>>,
    pub on_state_update: Option<Callback<State>>,
}

impl Default for Prop {
    fn default() -> Self {
        Self {
            state: State::default(),
            data: Ticker::default(),
            on_close: None,
            on_state_update: None,
        }
    }
}

impl Component for Modal {
    type Message = Msg;
    type Properties = Prop;

    fn create(prop: Self::Properties, mut link: ComponentLink<Self>) -> Self {
        link.send_self(Msg::Load);

        Modal {
            state: prop.state,
            ticker: prop.data,
            change_pct: 0.0,
            link: link,
            fetch: FetchService::new(),
            console: ConsoleService::new(),
            task: None,
            on_close: prop.on_close,
            on_state_update: prop.on_state_update,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Load => {
                self.fetch_data();
                false
            }
            Msg::Loaded(data) => {
                self.task = None;
                // process and load chart data
                let chart_points = self.process_data(data);
                self.change_pct = Self::calc_change_pct(&chart_points);
                self.load_chart(chart_points);
                true
            }
            Msg::Error(msg) => {
                self.console.error(&msg);
                false
            }
            Msg::Close => {
                if let Some(ref mut callback) = self.on_close {
                    callback.emit(());
                }
                true
            }
            Msg::Pin => {
                self.state.toggle_pinned(&self.ticker);
                if let Some(ref mut callback) = self.on_state_update {
                    callback.emit(self.state.clone());
                }
                true
            }
            Msg::IntervalSelect(interval) => {
                self.state.history_interval = interval;
                // update state
                if let Some(ref mut callback) = self.on_state_update {
                    callback.emit(self.state.clone());
                }
                self.fetch_data();
                false
            }
        }
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.state = props.state;
        self.ticker = props.data;
        self.on_close = props.on_close;
        self.on_state_update = props.on_state_update;
        true
    }
}

impl Renderable<Modal> for Modal {
    fn view(&self) -> Html<Self> {
        html! {
            <div id="ticker-modal", class="uk-modal-container uk-open uk-animation-slide-top-small", style="display:block", uk-modal="bg-close:false",>
                <div class="uk-modal-dialog uk-modal-body uk-margin-auto-vertical uk-background-secondary uk-light",>
                    // header
                    <div class="uk-modal-title text-tiny", uk-grid="",>
                        // pin icon
                        { pin_button(self.state.is_pinned(&self.ticker)) }
                        // coin name
                        <div class="uk-text-emphasis",>{ &self.ticker.name }</div>
                        // market information
                        { market_info(&self) }
                        // change percentage
                        { percentage(self.change_pct) }
                    </div>
                    <button class="uk-modal-close-default", uk-close="", onclick=|_| Msg::Close,/>

                    // chart
                    <div class="ct-chart ct-major-tenth", id="chart1",/>

                    // chart timerange selectors
                    { time_selectors(self.state.history_interval) }
                </div>
            </div>
        }
    }
}

fn pin_button(is_pinned: bool) -> Html<Modal> {
    let class = if is_pinned { "uk-text-success" } else { "" };
    let icon = if is_pinned { "lock" } else { "unlock" };
    html! {
        <a class={class}, uk-icon={icon}, onclick=|_| Msg::Pin, />
    }
}

fn market_info(modal: &Modal) -> Html<Modal> {
    // rounded price
    let price = *&modal.ticker.price_usd;
    let price = if price < 1.0 {
        round::ceil(price.into(), 4)
    } else {
        round::ceil(price.into(), 2)
    };

    let mcap = *&modal.ticker.market_cap_usd / 1_000_000_000.0;
    let market_long = format!(
        "Rank {} | Price ${} | Marketcap ${:.2}b",
        &modal.ticker.rank, &price, &mcap
    );
    let market_short = format!("${}", &price);

    html! {
        <>
            <div>
                <div class="uk-visible@s",>{ &market_long }</div>
                <div class="uk-hidden@s",>{ &market_short }</div>
            </div>
        </>
    }
}

fn percentage(pct: f32) -> Html<Modal> {
    let pct_class = if pct > 0.0 {
        "uk-text-success"
    } else {
        "uk-text-danger"
    };
    html! {
        <div class={pct_class},>{ &format!("{:+.2}%", pct) }</div>
    }
}

fn time_selectors(current: Interval) -> Html<Modal> {
    let get_class = |time: Interval| {
        if time == current {
            "uk-text-success"
        } else {
            ""
        }
    };
    html! {
        // weird flex, but ok
        <div class="uk-flex-center text-tiny", uk-grid="",>
            <a onclick=|_| Msg::IntervalSelect(Interval::Hour), class={get_class(Interval::Hour)},>{"Hour"}</a>
            <a onclick=|_| Msg::IntervalSelect(Interval::Day), class={get_class(Interval::Day)},>{"Day"}</a>
            <a onclick=|_| Msg::IntervalSelect(Interval::Week), class={get_class(Interval::Week)},>{"Week"}</a>
            <a onclick=|_| Msg::IntervalSelect(Interval::Month), class={get_class(Interval::Month)},>{"Month"}</a>
            <a onclick=|_| Msg::IntervalSelect(Interval::Year), class={get_class(Interval::Year)},>{"Year"}</a>
        </div>
    }
}

impl Modal {
    fn fetch_data(&mut self) {
        let end: u64 = js! {
            return Date.now();
        }
        .try_into()
        .expect("Failed to convert timestamp");
        let duration: std::time::Duration = self.state.history_interval.into();
        let start = end - duration.as_secs() * 1000;

        let interval = match self.state.history_interval {
            Interval::Hour => "m1",
            Interval::Day => "m15",
            Interval::Week => "h1",
            Interval::Month => "h6",
            Interval::Year => "d1",
        };
        self.console.log("fetch data");

        let get_history = Request::get(&format!(
            "https://api.coincap.io/v2/assets/{asset}/history?interval={interval}&start={start}&end={end}",
            asset = &self.ticker.id,
            interval = interval,
            end = end,
            start = start,
        ))
        .header("Accept-Encoding", "gzip")
        .body(Nothing)
        .expect("Failed to make request");

        let callback = self.link.send_back(
            move |response: Response<Json<Result<HistoryData, Error>>>| {
                let (meta, Json(data)) = response.into_parts();
                if meta.status.is_success() {
                    let data = data.expect("Failed to marshal response");
                    Msg::Loaded(data.data)
                } else {
                    Msg::Error(
                        meta.status
                            .canonical_reason()
                            .unwrap_or("Other error")
                            .into(),
                    )
                }
            },
        );
        self.task = Some(self.fetch.fetch(get_history, callback));
    }

    fn process_data(&self, data: Vec<History>) -> Vec<Point> {
        // Maximum number of points in chart
        let count = data.len();
        let resolution = 100;
        let step_size = (count / resolution).max(1);

        // Map to Point objects
        let points: Vec<Point> = data
            .into_iter()
            .step_by(step_size)
            .map(|h| Point {
                x: h.time as u64,
                y: h.price_usd.parse::<f32>().unwrap_or(0.0),
            })
            .collect();

        points
    }

    // calculates data change percentage
    fn calc_change_pct(data: &[Point]) -> f32 {
        // calculate change %
        if !data.is_empty() {
            let begin = data[0].y;
            let end = data[data.len() - 1].y;
            (end / begin - 1.0) * 100.0
        } else {
            0.0
        }
    }

    // Loads data to chart
    fn load_chart(&self, points: Vec<Point>) {
        let interval = self.state.history_interval;
        js! {
            new Chartist.Line("#chart1",
            {
                series: [@{points}]
            },
            {
                axisX: {
                    type: Chartist.FixedScaleAxis,
                    divisor: 8,
                    labelInterpolationFnc: function(v) {
                        switch (@{interval}) {
                            case "Hour":
                            case "Day":
                                return moment(v).format("HH:mm");
                            default:
                                return moment(v).format("l");
                        }
                    },
                },
                axisY: {
                    type: Chartist.FixedScaleAxis,
                    divisor: 5,
                    position: "end",
                    labelInterpolationFnc: function(n) {
                        if (n < 1) {
                            return Math.round(n * 10000) / 10000;
                        } else {
                            return Math.round(n * 100) / 100;
                        }
                    },
                },
                lineSmooth: Chartist.Interpolation.simple(),
                showPoint: false,
                showLine: true,
                showArea: true,
                chartPadding: {
                    top: 8,
                    left: 0,
                    right: 0,
                    bottom: 0,
                },
                fullWidth: true,
            },
            // responsive opts
            [
                ["screen and (max-width: 640px)", {
                    axisX: {
                        divisor: 5,
                    },
                }]
            ]
            );
        };
    }
}
