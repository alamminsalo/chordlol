use chords::util;
/**
 * Util functions
 */

// displays UIkit notification
#[allow(dead_code)]
pub fn show_notification(msg: &str, status: &str) {
    js! {
        UIkit.notification({
            message: @{msg},
            status: @{status},
            pos: "bottom-center",
            timeout: 3000
        });
    };
}

pub fn roman_numeral(index: usize) -> String {
    match index {
        1 => "I",
        2 => "II",
        3 => "III",
        4 => "IV",
        5 => "V",
        6 => "VI",
        7 => "VII",
        _ => "",
    }
    .into()
}

// normalizes note based on scale
pub fn normalize(note: &str, scale: &[String]) -> String {
    let note = note.to_lowercase();
    let alt_note = util::alt_note_str(note.clone());
    let scale: Vec<String> = scale.iter().map(|s| s.to_lowercase()).collect();
    if let Some(found) = scale.iter().find(|n| **n == note || **n == alt_note) {
        return found.clone();
    }
    note.into()
}
