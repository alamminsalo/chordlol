use crate::component::util;
use crate::component::Piano;
use chords::chord::Chord;
use yew::{html, Callback, Component, ComponentLink, Html, Renderable, ShouldRender};

pub struct Tile {
    index: usize,
    chords: Vec<Chord>,
    chord_index: usize,
    on_clicked: Option<Callback<Chord>>,
}

impl Tile {
    // returns chord with current index
    fn chord(&self) -> Chord {
        self.chords[self.chord_index].clone()
    }
}

pub enum Msg {
    SetChord(usize),
}

#[derive(PartialEq, Clone)]
pub struct Prop {
    pub index: usize,
    pub chords: Vec<Chord>,
    pub on_clicked: Option<Callback<Chord>>,
}

impl Default for Prop {
    fn default() -> Self {
        Self {
            index: 0,
            chords: vec![],
            on_clicked: None,
        }
    }
}

impl Component for Tile {
    type Message = Msg;
    type Properties = Prop;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        Tile {
            index: props.index,
            chords: props.chords,
            chord_index: 0,
            on_clicked: props.on_clicked,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::SetChord(chord_index) => {
                // clamp within bounds
                self.chord_index = {
                    if chord_index >= self.chords.len() {
                        0
                    } else {
                        chord_index
                    }
                };
            }
        }
        true
    }

    fn change(&mut self, props: Self::Properties) -> ShouldRender {
        self.on_clicked = props.on_clicked;
        self.chords = props.chords;
        true
    }
}

impl Renderable<Tile> for Tile {
    fn view(&self) -> Html<Self> {
        let roman = util::roman_numeral(self.index + 1);
        let chord = self.chord();

        html! {
            <div>
                <div class="uk-card uk-card-small uk-card-default",>
                    // title -- acts as clickable btn
                    <button class="uk-button uk-button-primary uk-light uk-width-expand",>
                        <div class="menu-toggle uk-grid-collapse uk-flex-middle", uk-grid="",>
                            // No
                            <small class="uk-text-primary noselect",>{roman}</small>
                            // Chord name
                            <div class="uk-text-capitalize uk-text-center uk-text-emphasis uk-width-expand noselect",>
                                {Self::render_name(&chord.name)}
                            </div>
                        </div>
                    </button>
                    // dropdown
                    { self.chords_menu() }
                    // chord piano view
                    <div class="uk-card-body uk-padding-remove uk-height-small",>
                        <Piano: chord=chord.clone(),/>
                    </div>
                </div>
            </div>
        }
    }
}

impl Tile {
    // renders name as chord + superscript part
    fn render_name(name: &str) -> Html<Self> {
        let parts: Vec<String> = name
            .split(|c| c == '(' || c == '+')
            .chain(vec![""]) // add extra element incase splitted part results in no superscript part
            .map(|s| s.replace(")", "")) // remove trailing ')'
            .collect();
        // assert!(parts.len() >= 2);

        html! {
            <span>{&parts[0]}</span><sup>{&parts[1]}</sup>
        }
    }

    // dropdown menu for selecting chord
    fn chords_menu(&self) -> Html<Self> {
        let current_index = self.chord_index;

        // closure for list entry
        let chord_entry = |(index, chord): (usize, &Chord)| -> Html<Self> {
            let class = if index == current_index {
                "uk-active"
            } else {
                ""
            };
            html! {
                <li class={class},>
                    <a href="#", class="noselect", onclick=|_| Msg::SetChord(index),>
                        { Self::render_name(&chord.name) }
                    </a>
                </li>
            }
        };
        html! {
            <div uk-dropdown="mode: click; pos: bottom-justify",>
                <ul class="uk-nav uk-dropdown-nav",>
                    { for self.chords.iter().enumerate().map(chord_entry) }
                </ul>
            </div>
        }
    }
}
