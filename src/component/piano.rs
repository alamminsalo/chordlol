use crate::component::util;
use chords::chord::Chord;

use yew::{html, Component, ComponentLink, Html, Renderable, ShouldRender};

pub struct Piano {
    notes: Vec<String>,
    chord_notes: Vec<String>,
    chord: Chord,
}

pub enum Msg {
    Play,
    Release,
    Nothing,
}

#[derive(PartialEq, Clone)]
pub struct Prop {
    pub chord: Chord,
}

impl Default for Prop {
    fn default() -> Self {
        Self {
            chord: Chord::default(),
        }
    }
}

impl Component for Piano {
    type Message = Msg;
    type Properties = Prop;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        // all notes for piano roll
        let notes = chords::analyze("C", "chromatic", false).0;

        // chord notes
        let chord = props.chord;

        // octave notes from chord
        let chord_notes = chord
            .notes
            .iter()
            .map(|n| {
                let note = util::normalize(n, &notes);
                format!("{}{}", &note.to_uppercase(), 4)
            })
            .collect();

        Piano {
            notes,
            chord_notes,
            chord,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::Play => {
                js! {
                    var notes = @{&self.chord_notes};
                    // play chord
                    window.mastersynth.triggerAttack(notes);
                };
            }
            Msg::Release => {
                js! {
                    // stop chord
                    window.mastersynth.releaseAll();
                }
            }
            _ => {}
        };

        false
    }

    fn change(&mut self, p: Self::Properties) -> ShouldRender {
        self.chord = p.chord;
        true
    }
}

impl Piano {
    // returns notes in a list of (white, <black>) keys
    // which can be composed easily in view
    fn notes_piano(&self) -> Vec<(String, Option<String>)> {
        // two iterators containing plain and sharp notes
        let mut black = self.notes.iter().filter(|n| n.contains("#")).peekable();
        let white = self.notes.iter().filter(|n| !n.contains("#"));

        // iterate to single vec, popping notes from black if note has sharp equivalent
        white
            .map(|w| {
                let w = w.clone();
                if let Some(b) = black.peek() {
                    if b.contains(&w) {
                        let b = black.next().unwrap().clone();
                        return (w, Some(b));
                    }
                }
                (w, None)
            })
            .collect()
    }
}

impl Renderable<Piano> for Piano {
    fn view(&self) -> Html<Self> {
        // returns class for key
        // depending if it is included in
        // the chord
        let key_class = |key: String| -> String {
            let mut class = "uk-position-bottom noselect".to_string();
            if !self.chord.notes.contains(&key) {
                class = format!("{} uk-hidden", &class);
            }
            class
        };

        // scoped functions for key arranging
        let black_key = |note: String| -> Html<Piano> {
            let note = util::normalize(&note, &self.chord.notes);
            html! {
                <div class="black uk-button-secondary uk-text-center uk-width-5-6",>
                <div class={key_class(note.clone())},>{ note.to_uppercase() }</div>
                </div>
            }
        };
        let white_key = |note: String, black: Option<String>| -> Html<Piano> {
            let note = util::normalize(&note, &self.chord.notes);
            html! {
                <div class="white uk-button-default uk-text-center",>
                    <div class={key_class(note.clone())},>{ note.to_uppercase() }</div>
                    { for black.map(black_key) }
                </div>
            }
        };

        html! {
            <div class="uk-button uk-height-1-1 text-tiny uk-button-default uk-padding-remove uk-width-expand", onmousedown=|_| Msg::Play, onmouseup=|_| Msg::Release,>
                <div class="piano uk-disabled uk-height-1-1 uk-flex uk-child-width-extend",>
                    { for self.notes_piano().into_iter().map(|(w,b)| white_key(w,b)) }
                </div>
            </div>
        }
    }
}
