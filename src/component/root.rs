use super::{Grid, Nav};
use crate::model::State;
use yew::services::storage::{Area, StorageService};
use yew::{html, Component, ComponentLink, Html, Renderable, ShouldRender};

const STORE_KEY: &'static str = "state.v1";

pub struct Root {
    state: State,
    storage: StorageService,
}

pub enum Msg {
    StateUpdate(State),
}

impl Component for Root {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        let mut storage = StorageService::new(Area::Local);
        let state: State = storage.restore(STORE_KEY);

        init_synth(&state);

        Root { storage, state }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::StateUpdate(state) => {
                self.storage.store(STORE_KEY, state);
                self.state = self.storage.restore(STORE_KEY);
                update_synth(&self.state);
            }
        }
        true
    }
}

impl Renderable<Root> for Root {
    fn view(&self) -> Html<Self> {
        html! {
            <div class="noselect",>
                <Nav: state =&self.state.clone(), on_state_change=|state| Msg::StateUpdate(state),/>
                <Grid: state=&self.state.clone(), on_state_change=|state| Msg::StateUpdate(state),/>
                </div>
        }
    }
}

// updates synth oscillator & envelope
fn update_synth(state: &State) {
    let oscillator = &state.oscillator;
    js! {
        try {
            // make new synth
            window.mastersynth.set({
                    oscillator: {
                        type: @{oscillator},
                    }
            });
        } catch(e) {
            console.log(e);
        }
    };
}

// inits synth by state
fn init_synth(state: &State) {
    let oscillator = &state.oscillator;
    js! {
        try {
            // make new synth
            var poly = new Tone.PolySynth(8, Tone.Synth)
                .set({
                    oscillator: {
                        type: @{oscillator},
                    },
                    envelope: {
                        attack: 0.005,
                        decay: 0.1,
                        sustain: 0.8,
                        release: 0.02
                    }
                })
                .unsync()
                .toMaster();
                // set global
            window.mastersynth = poly;
        } catch(e) {
            console.log(e);
        }
    };
}
