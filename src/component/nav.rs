use crate::model::State;
use chords::note::Note;
use chords::scale::Scale;
use yew::{html, Callback, Component, ComponentLink, Html, Renderable, ShouldRender};

pub enum Msg {
    SetRoot(Note),
    SetScale(Scale),
    SwitchWaveform,
}

pub struct Nav {
    pub state: State,
    pub on_state_change: Option<Callback<State>>,
}

#[derive(PartialEq, Clone)]
pub struct Prop {
    pub state: State,
    pub on_state_change: Option<Callback<State>>,
}

impl Default for Prop {
    fn default() -> Self {
        Self {
            state: State::default(),
            on_state_change: None,
        }
    }
}

impl Component for Nav {
    type Message = Msg;
    type Properties = Prop;

    fn create(prop: Self::Properties, _: ComponentLink<Self>) -> Self {
        Nav {
            state: prop.state,
            on_state_change: prop.on_state_change,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::SetRoot(root) => {
                self.state.root = root.into();
            }
            Msg::SetScale(scale) => {
                self.state.scale = scale.into();
            }
            Msg::SwitchWaveform => {
                self.state.oscillator = match &self.state.oscillator[..] {
                    "sine" => "triangle",
                    "triangle" => "sawtooth",
                    "sawtooth" => "square",
                    _ => "sine",
                }
                .into()
            }
        }
        if let Some(ref mut callback) = self.on_state_change {
            callback.emit(self.state.clone());
        }
        true
    }

    fn change(&mut self, prop: Self::Properties) -> ShouldRender {
        let changed = self.state != prop.state;
        self.state = prop.state;
        changed
    }
}

impl Renderable<Nav> for Nav {
    fn view(&self) -> Html<Self> {
        let notes = chords::analyze("C", "chromatic", false).0;
        let scales = chords::supported_scales();
        let selected_scale_notes = chords::analyze(&self.state.root, &self.state.scale, false).0;
        html! {
            <div class="navbar uk-animation-fade", uk-sticky="",>
                <nav class="uk-navbar-container uk-flex uk-background-primary",
                    uk-navbar="dropbar: true; mode: click",>
                    // logo
                    <div class="nav-toggle uk-navbar-item",
                        uk-toggle="target: .nav-toggle; animation: uk-animation-fade",>
                        <img uk-img="", data-src="logo.png", height="32", width="128", />
                    </div>
                    // navs
                    <div class="nav-toggle uk-navbar-item", hidden="",>
                        <ul class="uk-navbar-nav",>
                            // rootnote selector
                            <li>
                                <a class="uk-text-emphasis", href="#", uk-grid="",>
                                    <div>{&self.state.root}</div>
                                    <div>{&self.state.scale}</div>
                                    <div>{selected_scale_notes.join(" ")}</div>
                                </a>
                                <div class="uk-navbar-dropdown uk-navbar-dropdown-width-2",>
                                    <div class="uk-dropdown-grid uk-child-width-1-2", uk-grid="",>
                                        <div>
                                            <ul class="uk-padding-remove-top uk-padding uk-nav uk-navbar-dropdown-nav",>
                                                { for notes.into_iter().map(|s| self.note(s.into())) }
                                            </ul>
                                        </div>
                                        <div>
                                            <ul class="uk-padding-remove-top uk-padding uk-nav uk-navbar-dropdown-nav",>
                                                { for scales.into_iter().map(|s| self.scale(s.into())) }
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    // nav buttons
                    <div class="uk-navbar-item uk-width-expand uk-flex uk-flex-right",>
                        // osc component
                        { self.oscillator() }
                        // settings btn
                        <a uk-icon="settings", class="nav-toggle uk-navbar-toggle uk-text-emphasis", uk-toggle="target: .nav-toggle; animation: uk-animation-fade",/>
                        // close btn
                        <a uk-close="", class="nav-toggle uk-text-emphasis uk-navbar-toggle", hidden="", uk-toggle="target: .nav-toggle; animation: uk-animation-fade",/>
                    </div>

                </nav>
                 <div class="uk-navbar-dropbar",/>
            </div>
        }
    }
}

impl Nav {
    fn note(&self, note: Note) -> Html<Nav> {
        let note_str: String = note.into();
        let current_root: Note = self.state.root.clone().into();
        let class = if current_root == note {
            "uk-active"
        } else {
            ""
        };
        html! {
            <li class={class}, onclick=|_| Msg::SetRoot(note),>
                <a href="#",>{&note_str}</a>
            </li>
        }
    }

    fn scale(&self, scale: Scale) -> Html<Nav> {
        let scale_str: String = scale.into();
        let current_scale: Scale = self.state.scale.clone().into();
        let class = if current_scale == scale {
            "uk-active"
        } else {
            ""
        };
        html! {
            <li class={class}, onclick=|_| Msg::SetScale(scale),>
                <a class="uk-text-capitalize", href="#",>{&scale_str}</a>
            </li>
        }
    }

    fn oscillator(&self) -> Html<Nav> {
        let short = |osc: &str| -> String {
            match osc {
                "square" => "sqr".into(),
                _ => osc[..3].to_string(),
            }
        };
        html! {
            <a class="nav-toggle uk-navbar-toggle uk-text-emphasis", hidden="", onclick=|_| Msg::SwitchWaveform,>
                <button class="uk-button uk-margin-remove",>
                { short(&self.state.oscillator).to_uppercase() }
                </button>
            </a>
        }
    }
}
