#![recursion_limit = "256"]

#[macro_use]
extern crate stdweb;

pub mod component;
mod model;
